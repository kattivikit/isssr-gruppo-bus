package it.aggregator;

import java.util.List;

import it.model.Level2Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OperationAggregator {
	final static Logger logger = LoggerFactory
			.getLogger(OperationAggregator.class);

	public Level2Request aggregate(List<Level2Request> childRequests) {

		Level2Request out = new Level2Request(childRequests.get(0)
				.getOriginAdress(),
				childRequests.get(0).getDestinationAdress(), childRequests.get(
						1).getData(), childRequests.get(0).getId());
		logger.info("In Operation aggregator");
		return out;

	}
}
