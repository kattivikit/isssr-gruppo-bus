package it.splitter;

import java.util.ArrayList;
import java.util.List;
import org.springframework.messaging.Message;
import it.model.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContentSplitter {
	final static Logger logger = LoggerFactory.getLogger(ContentSplitter.class);

	public List<Request> splitMyMessageToRequests(Message<Request> message) {

		Request req = message.getPayload();
		List<Request> requests = new ArrayList<Request>();
		for (int i = 0; i < req.getContent().size(); i++) {
			ArrayList<String> contentExtracted = new ArrayList<String>();
			contentExtracted.add(req.extractContent(i));
			Request temp = new Request(req.getTag(), contentExtracted,
					req.getOriginAdress(), req.getResolvedAdress(), req.getId());
			requests.add(temp);
		}
		logger.info("In Content splitter, number of splitting: "
				+ requests.size());
		return requests;
	}
}
