package it.router;

import org.springframework.messaging.Message;

import it.model.Request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Level2Router {

	final static Logger logger = LoggerFactory.getLogger(Level2Router.class);

	public String select(Message<Request> message) {

		Request t = message.getPayload();
		logger.debug("Level 2 Router, TAG:" + t.getTag());
		if (t.getTag().equals("level2Data"))
		{
			logger.info("Level 2 Data Route");
			return "dataLevel2Channel";
		}
		if (t.getTag().equals("level2Operation"))
		{
			logger.info("Level 2 Operation Route");
			return "operationLevel2Channel";
		}
		logger.error("No-Route-Level 2,tag corrispondence error");
		return "nonMatchesChannel";
	}
}
