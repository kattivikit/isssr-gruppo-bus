package it.router;

import org.springframework.messaging.Message;
import it.model.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ERMESRouter {
	final static Logger logger = LoggerFactory
			.getLogger(ERMESRouter.class);
	public String select(Message<Request> message) {

		logger.debug("In ERMES Router, flag: "
				+ message.getPayload().getTag());
		Request t = message.getPayload();
		if (t.getOriginAdress().equals(""))
			return "updateChannel";
		else {

			if (t.getTag().equals("jDoraRequest")) {
				logger.info("Level3JDora-Route");
				return "jDoraChannel";
			}
			if (t.getTag().equals("level2")) {
				logger.info("Level2-Route");
				return "level2Channel";
			}

			if (t.getTag().equals("level1")) {
				logger.info("Level1-Route");
				return "level1Channel";
			}
			if (t.getTag().equals("level3Direct")) {
				logger.info("Level3-Route");
				return "level3Channel";
			}
			logger.error("No-Route,tag corrispondence error");
			return "nonMatchesChannel";

		}

	}
}
