package it.enricher;

import it.model.Request;

import org.springframework.messaging.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class EnricherService {
	final static Logger logger = LoggerFactory
			.getLogger(EnricherService.class);
public Request receive(Message<Request> message) {
		
		Request req= new Request();
		req.setResolvedAdress(message.getPayload().getResolvedAdress());
		req.setTag(message.getPayload().getTag());
		logger.info("In Enricher");
		return req;
	}
}
