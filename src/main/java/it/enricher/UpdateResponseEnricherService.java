package it.enricher;


import it.model.Request;
import org.springframework.messaging.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateResponseEnricherService {

	final static Logger logger = LoggerFactory
			.getLogger(UpdateResponseEnricherService.class);

	public String receive(Message<Request> message) {

		Request req = new Request();
		req.setContent(message.getPayload().getContent());
		logger.info("In Update-response Enricher");
		return message.getPayload().toString();
	}
}
