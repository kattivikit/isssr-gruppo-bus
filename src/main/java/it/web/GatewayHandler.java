package it.web;

import org.springframework.messaging.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GatewayHandler {
	final static Logger logger = LoggerFactory.getLogger(GatewayHandler.class);

	public Message<?> handleMessage(Message<?> message) {
		logger.debug("Request arrived from inbound Http Gateway and entered in Json to Object transformer \n");

		return message;
	}
}