package it.web;

import it.model.Request;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class HttpReceiver {
	final static Logger logger = LoggerFactory.getLogger(HttpReceiver.class);

	@ServiceActivator
	public Message<Request> receive(Message<Request> message) {
		logger.debug("Request arrived from Inbound Adapter and entered in Json to Object transformer\n request type:");

		return message;
	}

	// ritorna il messaggio che deve essere inviato come risposta
}
