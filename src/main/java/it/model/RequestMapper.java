package it.model;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RequestMapper implements RowMapper<Request> {

	/* (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */	
	public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
		Request request = new Request();
		request.setTag(rs.getString("tag"));
		request.setResolvedAdress(rs.getString("resolvedAdress"));
				return request;
	}
}
