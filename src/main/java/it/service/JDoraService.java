package it.service;

import it.model.Request;
import org.springframework.messaging.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class JDoraService {
	final static Logger logger = LoggerFactory
			.getLogger(JDoraService.class);
	public Message<Request> receive(Message<Request> message) {
		
		logger.info("In Jdora service");
		Request req= message.getPayload();
		if(req.getOriginAdress().equals(req.getResolvedAdress()))
			message.getPayload().setResolvedAdress("null");
		return message;
	}
}