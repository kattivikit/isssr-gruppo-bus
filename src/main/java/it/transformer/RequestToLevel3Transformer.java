package it.transformer;

import it.model.Request;
import it.model.Level3Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestToLevel3Transformer {
	final static Logger logger = LoggerFactory
			.getLogger(RequestToLevel3Transformer.class);

	public Level3Request transform(Request t) {
		Level3Request req = new Level3Request(t.getTag(),
				t.getContent().get(0), null,
				t.getResolvedAdress(), null, null, null);
		logger.info("In RequestToLevel3Transformer");
		return req;
	}
}