package it.transformer;

import java.util.ArrayList;

import it.model.Request;
import it.model.Level3Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Level3ToRequestTransformer {
	
	final static Logger logger = LoggerFactory
			.getLogger(Level3ToRequestTransformer.class);
	
	public Request transform(Level3Request t) {
		ArrayList<String> content = new ArrayList<String>();
		if (t.getData() != null)
			content.add(t.getData());
		Request req = new Request(t.getTag(), content, "", "", t.getId());
		logger.info("in Level3ToRequestTransformer");
		return req;
	}
}