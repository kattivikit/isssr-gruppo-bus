package it.transformer;

import java.util.ArrayList;

import it.model.Request;
import it.model.Level2Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Level2ToRequestTransformer {

	final static Logger logger = LoggerFactory
			.getLogger(Level2ToRequestTransformer.class);

	public Request transform(Level2Request t) {
		Request req;
		ArrayList<String> content = new ArrayList<String>();
		if (t.getData() != null) {
			for (int i = 0; i < t.getData().size(); i++) {
				String num = t.getData().get(i);
				content.add(num.toString());
			}
			req = new Request(null, content, t.getOriginAdress(),
					t.getDestinationAdress(), t.getId());

			logger.info("in  Level2ToRequestTransformer-Data option");
		} else {
			// se il campo data è vuoto vuol dire che il prodotto
			// dell'operazione è raggiungibile attraverso l'indirizzo presente
			// in destinationAdress
			req = new Request(null, null, t.getOriginAdress(),
					t.getDestinationAdress(), t.getId());
			logger.info("in  Level2ToRequestTransformer-Link option");;
		}
		return req;
	}
}