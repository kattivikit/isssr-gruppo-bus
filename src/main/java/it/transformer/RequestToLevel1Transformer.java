package it.transformer;

import it.model.Level1Request;
import it.model.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestToLevel1Transformer {

	final static Logger logger = LoggerFactory
			.getLogger(RequestToLevel1Transformer.class);

	public Level1Request transform(Request t) {
		Level1Request req = new Level1Request(t.getOriginAdress(),
				t.getResolvedAdress(), t.getContent(), t.getId());
		logger.info("In RequestToLevel1Transformer");
		return req;
	}
}