package it.transformer;

import it.model.Level1Request;
import it.model.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Level1ToRequestTransformer {

	final static Logger logger = LoggerFactory
			.getLogger(Level1ToRequestTransformer.class);
	public Request transform(Level1Request t) {
		Request req = new Request(null, t.getData(),t.getDestinationAdress(),t.getOriginAdress(), t.getId());
		logger.info("In Level1ToRequestTransformer");
		return req;
	}
}