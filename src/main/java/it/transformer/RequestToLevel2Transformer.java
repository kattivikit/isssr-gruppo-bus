package it.transformer;

import it.model.Request;
import it.model.Level2Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestToLevel2Transformer {
	final static Logger logger = LoggerFactory
			.getLogger(RequestToLevel2Transformer.class);

	public Level2Request transform(Request t) {
		Level2Request req = new Level2Request(t.getOriginAdress(),
				t.getResolvedAdress(), null, t.getId());
		logger.info("In RequestToLevel2Transformer");
		logger.debug("In RequestToLevel2Transformer-adress="
				+ t.getResolvedAdress());
		return req;
	}
}